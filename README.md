# Intelligent Robotics Labs

Este es el repo principal de las actividades del Intelligent Robotics Labs (@IntellRobotLabs), 
que forma parte del Grupo de Robótica de la Universidad Rey Juan Carlos (@RoboticsLabURJC).

## Admins

- Francisco Martín - francisco.rico@urjc.es
- Jonathan Ginés - jgines@gsyc.urjc.es
- David Vargas - dvargas@gsyc.urjc.es

## [Wiki](https://gitlab.com/fmrico/intelligent_robotics_labs/wikis/Home)

